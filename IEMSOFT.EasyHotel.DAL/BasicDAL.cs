﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL
{
   public class BasicDAL:IDisposable
    {
       private PetaPoco.Database _db;
       protected PetaPoco.Database DB { get {return _db; } }
       public BasicDAL()
       {
           _db = new PetaPoco.Database("EasyHotel");
           _db.KeepConnectionAlive = true;
       }

       public BasicDAL(bool shouldKeepAlive)
       {
           _db = new PetaPoco.Database("EasyHotel");
           _db.KeepConnectionAlive = shouldKeepAlive;
       }

       public void Dispose()
       {
           try
           {
               _db.CloseSharedConnection();
           }
           catch (Exception)
           {
               
              //ignore
           }
       }
    }
}
