﻿using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.Admin.Filters;

namespace IEMSOFT.EasyHotel.Admin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ExceptionFilterAttribute());
            filters.Add(new AuthorizeFilterAttribute());
        }
    }
}