﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class BillCreateModel:BillBasicModel
    {
        public int CreatedByUserId { get; set; }
    }
}