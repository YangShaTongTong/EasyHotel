﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class PrintDayReportModel 
    {
        public string FromDate { get; set; }
        public string EndDate { get; set; }
        public List<BillFullModel> Bills  { get; set; }
        public decimal RoomFeeSum { get; set; }
        public decimal ConsumeFeeSum { get; set; }
        public decimal TotalFee { get; set; }
    }
}
